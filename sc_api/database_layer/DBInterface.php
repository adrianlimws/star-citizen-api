<?php

namespace thalos_api;

use PDO;

abstract class TableDirectory
{
    // RSI
    
        // Organizations
            const RSIOrgsTable = 'organizations_rsi';
            const RSIOrgsInfoTable = 'organizations_rsi_info';
            const RSIOrgsMembersTable = 'organizations_rsi_members';
            
        // Accounts
            const RSIAccountsTable = 'accounts_rsi';
            const RSIAccountsInfoTable = 'accounts_rsi_info';
            
            
    // API
    
        // Stats
            
        // Users
            const APIUsersTable = 'api_users';
            
    public static function ValidateTable($table)
    {
        switch($table)
        {
            case TableDirectory::RSIOrgsTable:
            case TableDirectory::RSIOrgsInfoTable:
            case TableDirectory::RSIOrgsMembersTable:
            case TableDirectory::RSIAccountsTable:
            case TableDirectory::RSIAccountsInfoTable:
            case TableDirectory::APIUsersTable:
                return $table;
        }
        
        return null;
    }
}

class DBInterface
{
    protected $db;
    protected $database;
    protected $hostname;
    protected $username;
    
    private $getLayeredRows_RSI_OrgsInfo;
    private $getLayeredRows_RSI_AllOrgsInfo;
    private $getLayeredRows_RSI_OrgMembers;
    private $getLayeredRows_RSI_OrgMember;
    private $getLayeredRows_RSI_AccountsInfo;
    private $getLayeredRows_RSI_AllAccountsInfo;
    
    public function __set($name, $value)
    {
        // Make sure nothing overwrites our connection settings.
        // Everything else is fair game.
        switch($name)
        {
            case 'hostname':
            case 'username':
            case 'database':
            case 'db_connection':
                break;
            default:
                $this->$name = $value;
                break;
        }
    }
    
    public function __get($name)
    {
        return $this->$name;
    }
    
    public function Connect()
    {
        try 
        {
            require(__DIR__.'/../settings.php');
            
            // Create a new connection with the supplied settings
            $this->db = new PDO(
                    'mysql:host='.$_SETTINGS['database']['host'].';dbname='.$_SETTINGS['database']['database'], 
                    $_SETTINGS['database']['username'], 
                    $_SETTINGS['database']['password']);
            
            
            $this->database = $_SETTINGS['database']['database'];
            $this->hostname = $_SETTINGS['database']['host'];
            $this->username = $_SETTINGS['database']['username'];
            
            // Set up our pre-defined queries
            $this->SetQueries();
            
            return true;
        }
        catch(PDOException $e) 
        {
            // Make sure everything gets reset on failure
            $this->Disconnect();
        }
        
        return false;
    }
    
    public function Disconnect()
    {
        // Disconnect from the database
        $this->db = null;
        
        // Reset all our connection settings
        $this->database = null;
        $this->hostname = null;
        $this->username = null;
        
        return true;
    }
    
    private function SetQueries()
    {
        // If we're connected to the database
        if($this->db != null)
        {
            // Returns all the organizations in the database
            $this->getLayeredRows_RSI_AllOrgsInfo = $this->db->prepare(
                'SELECT 
                    * 
                FROM 
                    '.TableDirectory::RSIOrgsTable.'
                WHERE 
                    last_scrape_date <= :end_date
                ORDER BY
                    id ASC');
            
            // Returns a specific organization's data
            $this->getLayeredRows_RSI_OrgsInfo = $this->db->prepare(
                'SELECT 
                    * 
                FROM 
                    '.TableDirectory::RSIOrgsInfoTable.'
                WHERE 
                        sid = :target
                    AND
                        scrape_date <= :end_date
                ORDER BY
                    scrape_date ASC');
            
            // Returns all of a specified organization's members
            $this->getLayeredRows_RSI_OrgMembers = $this->db->prepare(
                'SELECT 
                    * 
                FROM 
                    '.TableDirectory::RSIOrgsMembersTable.'
                WHERE 
                        sid = :sid
                    AND
                        scrape_date <= :end_date
                ORDER BY
                    handle ASC');
            
            // Returns a specific organization member's data
            $this->getLayeredRows_RSI_OrgMember = $this->db->prepare(
                'SELECT 
                    * 
                FROM 
                    '.TableDirectory::RSIOrgsMembersTable.'
                WHERE 
                        sid = :sid
                    AND
                        handle = :handle
                    AND
                        scrape_date <= :end_date
                ORDER BY
                    scrape_date ASC');
            
            // Returns all user accounts
            $this->getLayeredRows_RSI_AllAccountsInfo = $this->db->prepare(
                'SELECT 
                    * 
                FROM 
                    '.TableDirectory::RSIAccountsTable.'
                WHERE 
                        last_scrape_date <= :end_date
                ORDER BY
                    id ASC');
            
            // Returns a specific user account's data
            $this->getLayeredRows_RSI_AccountsInfo = $this->db->prepare(
                'SELECT 
                    * 
                FROM 
                    '.TableDirectory::RSIAccountsInfoTable.'
                WHERE 
                        handle = :target
                    AND
                        scrape_date <= :end_date
                ORDER BY
                    scrape_date ASC');
        }
    }
    
    public function GetTargetRows($table, $target, $end_date, $target_2 = 1)
    {
        // If we're connected to the database
        if($this->db != null)
        {
            try
            {
                // Grab the pre-made query appropriate for the supplied table name
                switch($table)
                {
                    case TableDirectory::RSIOrgsTable:
                        $stmt = $this->getLayeredRows_RSI_AllOrgsInfo;
                        break;
                    
                    case TableDirectory::RSIOrgsInfoTable:
                        $stmt = $this->getLayeredRows_RSI_OrgsInfo;
                        $stmt->bindParam(':target', $target);
                        break;
                    
                    case TableDirectory::RSIOrgsMembersTable:
                        
                        // If our second target is at default
                        if($target_2 == 1)
                        {
                            $stmt = $this->getLayeredRows_RSI_OrgMembers;
                            $stmt->bindParam(':sid', $target);
                        }
                        else
                        {
                            $stmt = $this->getLayeredRows_RSI_OrgMember;
                            $stmt->bindParam(':sid', $target);
                            $stmt->bindParam(':handle', $target_2);
                        }
                        break;
                        
                    case TableDirectory::RSIAccountsInfoTable:
                        $stmt = $this->getLayeredRows_RSI_AccountsInfo;
                        $stmt->bindParam(':target', $target);
                        break;
                    
                    case TableDirectory::RSIAccountsTable:
                        $stmt = $this->getLayeredRows_RSI_AllAccountsInfo;
                        break;
                    
                    default:
                        // Return failure if the table is improper
                        return false;
                }
                
                // Set our end date for all queries
                $stmt->bindParam(':end_date', $end_date);
                
                // Run the query
                $stmt->execute();
                
                // Grab all the data
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                
                // Return our data
                return $result;
            }
            catch(PDOException $e)
            {
                // Fall through to return failure
            }
        }
        
        return false;
    }
    
    public function RebuildHistory($table, $target, $end_date, $target_2 = 1)
    {
        // If we're connected to the database
        if($this->db != null)
        {
            // Make sure the table name is valid
            if(TableDirectory::ValidateTable($table) != null)
            {
                // Get the data for the pre-made query appropriate for the specified table
                $result = $this->GetTargetRows($table, $target, $end_date, $target_2);

                // Set minimum data
                $last_scrape_date = 0;

                // If we managed to find any data
                if($result != false)
                {
                    $output = array();
                    
                    // Get each of the data's rows
                    foreach($result as $row)
                    {
                        // For each of the row's values
                        foreach($row as $key=>$value)
                        {
                            // If the value isn't empty
                            if($value != null
                                && $value != '')
                            {
                                // Overwrite any previous value in this column
                                $output[$key] = $value;
                            }

                            // Each record has a last_scrape_date so we
                            // need to manually update the running max
                            if($key == 'last_scrape_success')
                            {
                                if($value > $last_scrape_date)
                                {
                                    $last_scrape_date = $value;
                                }
                            }
                        }
                    }

                    return array($output, $last_scrape_date);
                }
            }
        }
        
        // Something went wrong
        return false;
    }
    
    public function Diff($table, $target, $data, $target_2 = 1)
    {
        // Get the latest data for the target
        list($existing_data, $last_scrape_date) = $this->RebuildHistory($table, $target, time(), $target_2);
        
        if($data != null)
        {
            // For each value in our supplied data
            foreach($data as $key=>$value)
            {
                // If the value is an array
                if(gettype($value) == 'array')
                {
                    // Turn it into a string and compare it to our database value.
                    // If they match, blank our the matching column in our return data
                    if(implode(',',$value) == $existing_data[$key])
                    {
                        $data[$key]='';
                    }
                }
                // Else if the value is a scalar data type
                else
                {
                    // Compare it to our database value.
                    // If they match, blank our the matching column in our return data
                    if($value == $existing_data[$key])
                    {
                        $data[$key]='';
                    }
                }
            }
        }
        
        // Return an array where any values that are different are not null
        return $data;
    }
    
    public function BuildInsertQuery($table, $table_cols)
    {
        // Make sure the table name is valid
        if($table = TableDirectory::ValidateTable($table))
        {
            $query_string = 'INSERT INTO '.$table.' (';
            
                foreach($table_cols as $item)
                {
                    $query_string.=$item.',';
                }
                $query_string = substr($query_string, 0, -1);
                
            $query_string.=') VALUES (';
            
                foreach($table_cols as $item)
                {
                    $query_string.=':'.$item.',';
                }
                $query_string = substr($query_string, 0, -1);
                
            $query_string.=')';
            
            // If our database connection can prepare our new query
            if($query = $this->db->prepare($query_string))
            {
                return $query;
            }
        }
        
        return false;
    }
    
    public function CheckReference($table, $target)
    {
        // Make sure the table name is valid
        if($table = TableDirectory::ValidateTable($table))
        {
            // Check for any matching entry in the supplied table
            $query_string = 'SELECT * FROM '.$table.' WHERE id = :target LIMIT 1';
            
            // If our database connection can prepare our new query
            if($query = $this->db->prepare($query_string))
            {
                $query->bindParam(':target', $target);
                
                $query->execute();
                
                // If we get any data back
                if($query->fetch())
                {
                    // Return success
                    return true;
                }
            }
        }
        
        // Return failure
        return false;
    }
    
    public function UpdateUser($ip)
    {
        // Check for any matching entry in the supplied table
        $query_string = 'SELECT * FROM '.TableDirectory::APIUsersTable.' WHERE ip = :ip LIMIT 1';

        // If our database connection can prepare our new query
        if($query = $this->db->prepare($query_string))
        {
            $query->bindParam(':ip', $ip);

            $query->execute();

            // If we get any data back
            if($row = $query->fetch(PDO::FETCH_ASSOC))
            {
                $query_string = 'UPDATE '.TableDirectory::APIUsersTable.' SET 
                    date_last_seen = :time, times_seen = :seen WHERE ip = :ip';
                
                if($query = $this->db->prepare($query_string))
                {
                    $query->bindParam(':ip', $ip);
                    $time = time(); // To appease warning output
                    $query->bindParam(':time', $time);
                    $seen = $row['times_seen'] + 1; // Cause, PDO...
                    $query->bindParam(':seen', $seen);

                    return $query->execute();
                }
            }
            else
            {
                $query_string = 'INSERT INTO '.TableDirectory::APIUsersTable.' 
                    (ip, date_first_seen, date_last_seen, times_seen)
                    VALUES
                    (:ip, :time, :time, 1)';
                
                if($query = $this->db->prepare($query_string))
                {
                    $query->bindParam(':ip', $ip);
                    $time = time(); // To appease warning output
                    $query->bindParam(':time', $time);

                    return $query->execute();
                }
            }
        }
        
        return false;
    }
}