<?php

namespace thalos_api;

if(count($_GET) > 0)
{
    if($_GET['redirect'] == 'tools')
    {
        include_once('templates/tools.php');
    }
    else
    {
        require_once(__DIR__.'/../application_layer/Controller.php');
        
        $API = new Controller();

        header('Content-type: application/json');
        echo json_encode($API->Query($_GET), JSON_PRETTY_PRINT);
    }
}
else
{
    include_once('templates/front_page.php');
}