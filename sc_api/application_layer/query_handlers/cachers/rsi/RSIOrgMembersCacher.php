<?php

namespace thalos_api;

require_once(__DIR__.'/../Cacher.php');

class RSIOrgMembersCacher extends Cacher
{
    public function PerformQuery()
    {
        $target_id = $this->Output->request_stats->resolved_query->target_id;
        
        $result = $this->DB->GetTargetRows(TableDirectory::RSIOrgsMembersTable, $target_id, time());
        
        $data = array();
        $seen = array();
        foreach($result as $entry)
        {
            if(!in_array($entry['handle'], $seen))
            {
                list($history, $last_scrape) = $this->DB->RebuildHistory(TableDirectory::RSIOrgsMembersTable, $target_id, time(), $entry['handle']);
                
                unset($history['entry_id']);
                unset($history['sid']);
                
                $data[] = $history;
                
                $seen[] = $entry['handle'];
            }
        }
        
        $this->Output->SetData($data);
        
        return parent::PerformQuery();
    }
    
    protected function ValidateLocalSettings()
    { }
    
    public function UpdateCache($targets, $sender)
    { }
}