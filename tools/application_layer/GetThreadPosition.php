<?php

class GetThreadPosition
{
    public static function Query($target_forum, $target_thread)
    {
        $current_page = 1;
        $end_page = 999;
        $page_range = 10;
        $items_per_page = 30;
        
        while($current_page < $end_page)
        {
            $api_query='http://sc-api.com?system=forums&action=threads&target_id='.$target_forum
                    .'&start_page='.$current_page
                    .'&end_page='.($current_page==1? $current_page: $current_page + $page_range)
                    .'&expedite=1';

            $data = json_decode(file_get_contents($api_query), true);
            
            echo 'Currently Disabled.';
            return 'Currently Disabled.';
            
            $data = $data['data'];
            
            if($data == null)
            {
                return false;
            }
            
            $i=1;
            foreach($data as $thread)
            {
                if($thread['thread_id'] == $target_thread)
                {
                    $results['position'] = GetThreadPosition::GetFoundPosition($current_page, $i, $items_per_page);
                    $results['page'] = GetThreadPosition::GetFoundPage($current_page, $i, $items_per_page);
                    
                    return json_encode($results);
                }
                
                $i++;
            }
            
            $current_page += ($current_page==1? 1: $page_range);
        }
    }
    
    private static function GetFoundPosition($current_page, $i, $items_per_page)
    {
        if($current_page == 1)
        {
            return $i;
        }
        else if($i%$items_per_page == 0)
        {
            return $items_per_page;
        }
        else
        {
            return floor($i % $items_per_page);
        }
        
        return 0;
    }
    
    private static function GetFoundPage($current_page, $i, $items_per_page)
    {
        if($current_page == 1)
        {
            return 1;
        }
        else
        {
            return floor($i / $items_per_page) + $current_page;
        }
        
        return 0;
    }
}

