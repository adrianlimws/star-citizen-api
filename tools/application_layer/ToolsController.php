<?php

require_once('GetUserAvatar.php');
require_once('GetThreadPosition.php');
require_once('ValidateAccount.php');

class ToolsController
{
    public function Query($query)
    {
        set_time_limit(300);
        
        switch($query['system'])
        {
            case 'avatar':
                echo GetUserAvatar::Query($query['target_id']);
                break;
            
            case 'thread_position':
                echo GetThreadPosition::Query($query['target_id'], $query['target_id_2']);
                break;
            
            case 'validate_account':
                echo ValidateAccount::Query($query['target_id']);
                break;
        }
    }
}