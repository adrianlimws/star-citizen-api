<?php

class GetUserAvatar
{
    public static function Query($target)
    {
        $target = str_replace(array('.png','.jpg','.jpeg','.gif'), '', $target);
        
        $api_query='http://sc-api.com?system=accounts&action=dossier&api_source=cache&target_id='.$target.'&expedite=1';
        
        $data = json_decode(file_get_contents($api_query), true);
        $data = $data['data'];
        
        if(isset($data['avatar']) == null)
        {
            $data['avatar'] = 'http://robertsspaceindustries.com/rsi/static/images/account/avatar_default_big.jpg';
        }
        
        return GetUserAvatar::PrintImage($data['avatar']);
    }
    
    public static function PrintImage($url)
    {
        header('Content-type: image/png');
        
        return @readfile($url);
    }
}

